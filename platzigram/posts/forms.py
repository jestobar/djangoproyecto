"""post forms"""

#django

from django import forms

#models
from posts.models  import Post




class PostForm(forms.ModelForm):
    """Poste  model form"""
    
    class Meta:
        """form settigns"""
        model = Post
        fields =('user','profile','title', 'photo')
        
    