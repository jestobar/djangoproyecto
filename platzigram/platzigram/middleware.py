"""platzigram middleware catalog"""
#django 
from django.shortcuts import redirect
from django.urls import reverse

class ProfileCompletionMiddleware:
    """Profile completion middleware.

    Ensure every user that is interacting with the platform
    have their profile picture and biography.
    """

    def __init__(self, get_response):
        """Middleware initialization."""
        self.get_response = get_response

    def __call__(self, request):
        
        if not request.user.is_anonymous:
            
            #valida que el usuario sea administrador y permite que la solicitud continue
            if request.user.is_superuser:
                return self.get_response(request)
            
            #verifica el perfil del usuario
            if not request.user.is_staff:
                profile = request.user.profile
                if not profile.picture or not profile.biography:
                    if request.path not in [reverse('users:update'), reverse('users:logout')]:
                        return redirect('users:update')

        response = self.get_response(request)
        return response 